## Installation

```bash
git clone git@gitlab.com:sreehari.k1/tokigames-pilot.git
cd tokigames-pilot
yarn
```

## Get started

```bash
yarn start
```

## To Build

```bash
yarn build
```
