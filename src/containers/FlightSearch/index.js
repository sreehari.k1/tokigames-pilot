import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { createStructuredSelector } from 'reselect';
import {
  makeSelectCheapFlights,
  makeSelectBusinessFlights,
  makeSelectPlaces,
  makeSelectFilters,
  makeSelectIsLoading,
} from '../../redux/FlightSearch/selectors';
import {
  loadCheapFlights,
  loadBusinessFlights,
  updateSearchFilters,
} from '../../redux/FlightSearch/actions';

import SearchForm from '../../components/FlightSearch/SearchForm';
import ListTable from '../../components/FlightSearch/ListTable';
import LoadingIndicator from '../../components/LoadingIndicator';

function FlightSearch({
  cheapFlights,
  businessFlights,
  places,
  filters,
  onLoadCheapFlights,
  onLoadBusinessFlights,
  updateSearchFilters,
  isLoading,
}) {
  const allFlights = _.filter(
    _.union(
      _.map(cheapFlights, flight => _.extend(flight, { category: "Cheap" })),
      _.map(businessFlights, flight => _.extend(flight, { category: "Business" }))
    ),
    (flight) => {
      if (filters && filters.source) {
        if (flight.source !== filters.source) {
          return false;
        }
      }
      if (filters && filters.destination) {
        if (flight.destination !== filters.destination) {
          return false;
        }
      }
      if (filters && filters.category) {
        if (flight.category !== filters.category) {
          return false;
        }
      }
      return true;
    }
  );

  const returnFlights = _.filter(
    _.union(
      _.map(cheapFlights, flight => _.extend(flight, { category: "Cheap" })),
      _.map(businessFlights, flight => _.extend(flight, { category: "Business" }))
    ),
    (flight) => {
      if (filters && filters.source) {
        if (flight.source !== filters.destination) {
          return false;
        }
      }
      if (filters && filters.destination) {
        if (flight.destination !== filters.source) {
          return false;
        }
      }
      if (filters && filters.category) {
        if (flight.category !== filters.category) {
          return false;
        }
      }
      return true;
    }
  );

  useEffect(() => {
    onLoadCheapFlights();
    onLoadBusinessFlights();
  }, []);

  return (
    <Grid container>
      {isLoading ? <LoadingIndicator /> :
        <>
          <Grid item xs={4}>
            <SearchForm places={places} onUpdateSearchFilters={updateSearchFilters} />
          </Grid>
          <Grid item xs={8}>
            <ListTable flights={allFlights} />
            {filters.source && filters.destination && filters.trip === "two" ? <>
              <Typography variant="h5">Return Flights</Typography>
              <ListTable flights={returnFlights} />
            </> : ''}
          </Grid>
        </>
      }
    </Grid>
  );
}

FlightSearch.propTypes = {
  cheapFlights: PropTypes.array.isRequired,
  businessFlights: PropTypes.array.isRequired,
  places: PropTypes.array.isRequired,
  filters: PropTypes.object.isRequired,
  onLoadCheapFlights: PropTypes.func.isRequired,
  onLoadBusinessFlights: PropTypes.func.isRequired,
  updateSearchFilters: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  cheapFlights: makeSelectCheapFlights(),
  businessFlights: makeSelectBusinessFlights(),
  places: makeSelectPlaces(),
  filters: makeSelectFilters(),
  isLoading: makeSelectIsLoading(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onLoadCheapFlights: () => dispatch(loadCheapFlights()),
    onLoadBusinessFlights: () => dispatch(loadBusinessFlights()),
    updateSearchFilters: (filter) => dispatch(updateSearchFilters(filter)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(FlightSearch);
