import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const travelClass = [
  { key: '', value: 'All' },
  { key: 'Cheap', value: 'Cheap' },
  { key: 'Business', value: 'Business' },
];

const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: 200,
    },
  },
  buttons: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

function SearchForm({ places, onUpdateSearchFilters }) {
  const classes = useStyles();
  const [source, setSource] = useState("");
  const [destination, setDestination] = useState("");
  const [category, setCategory] = useState("");
  const [trip, setTrip] = useState("one");

  useEffect(() => {
    if (!source || !destination) {
      setTrip("one");
    }
  }, [ source, destination ]);

  const handleSubmit = () => {
    onUpdateSearchFilters({
      source,
      destination,
      category,
      trip
    });
  };

  const handleReset = () => {
    setSource("");
    setDestination("");
    setCategory("");
    setTrip("one");
    onUpdateSearchFilters({
      source: "",
      destination: "",
      category: "",
      trip: "one"
    });
  };

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <TextField
          id="source-location"
          name="sourceLocation"
          select
          value={source}
          label="Source"
          helperText="Please select your source"
          onClick={(e) => setSource(e.target.value)}
        >
          {["All", ...places].map(option => (
            <MenuItem key={option} value={option === "All" ? "" : option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          id="destination-location"
          name="destinationLocation"
          select
          value={destination}
          label="Destination"
          helperText="Please select your destination"
          onClick={(e) => setDestination(e.target.value)}
        >
          {["All", ...places].map(option => (
            <MenuItem key={option} value={option === "All" ? "" : option}>
              {option}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          id="travel-class"
          name="travelClass"
          select
          value={category}
          label="Class"
          helperText="Please select your class"
          onClick={(e) => setCategory(e.target.value)}
        >
          {travelClass.map(option => (
            <MenuItem key={option.value} value={option.key}>
              {option.value}
            </MenuItem>
          ))}
        </TextField>
        <RadioGroup name="trip" value={trip} onChange={e => setTrip(e.target.value)} row>
          <FormControlLabel
            value="one"
            control={<Radio color="primary" />}
            label="Oneway trip"
            labelPlacement="top"
            disabled={!(source && destination)}
          />
          <FormControlLabel
            value="two"
            control={<Radio color="primary" />}
            label="Round trip"
            labelPlacement="top"
            disabled={!(source && destination)}
          />
        </RadioGroup>
        <div className={classes.buttons}>
          <Button variant="contained" color="primary" onClick={handleSubmit}>Filter</Button>
          <Button variant="contained" onClick={handleReset}>Reset</Button>
        </div>
      </div>
    </form>
  );
}

SearchForm.propTypes = {
  places: PropTypes.array.isRequired,
  onUpdateSearchFilters: PropTypes.func.isRequired,
};

export default SearchForm;
